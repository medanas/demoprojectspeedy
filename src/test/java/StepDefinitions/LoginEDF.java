package StepDefinitions;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginEDF {
	
	
	WebDriver driver = new ChromeDriver();
	
	@Given("user navigate to page login EDF")
	public void user_navigate_to_page_login_edf() {
	    System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
	    
	    
	    
	    driver.get("https://edf.test.hse-compliance.net/?p=101&sp=0");
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}

	@And("user enters username and password")
	public void user_enters_username_and_password() {
		
		driver.findElement(By.id("txtUserName")).sendKeys("sana.bakkari@infopro-digital.com");
		driver.findElement(By.id("txtUserPass")).sendKeys("**Welcome**2020**");
		
	}

	@When("user clicks on connexion botton")
	public void user_clicks_on_connexion_botton() {
		driver.findElement(By.id("meConnecter")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	   
	}

	@Then("user is navigated to the EDF platform")
	public void user_is_navigated_to_the_edf_platform() {
		String ExpectedResult = driver.findElement(By.className("header__logo__txt")).getText();
	   Assert.assertTrue("C.N.P.E. DE CIVAUX",ExpectedResult.contains(ExpectedResult) );
	   //driver.close();
	}

}
